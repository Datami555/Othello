﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public partial class ChessBoard
{
    int Evaluate(TypePiece[,] myBoard, TypePiece myTypePiece)
    {
        int pieces = PiecesOnBoard(myBoard);
        if (pieces <= 20)
        {
            //opening game
            return 10 * Mobility(myBoard, myTypePiece) +
                   50 * Corners(myBoard, myTypePiece) +
                   10 * StablePiecesFromCorners(myBoard, myTypePiece) +
                        NotMove(myBoard,myTypePiece);

        }
        else if (pieces <= 56)
        {
            //mid game
            return 20 * Mobility(myBoard, myTypePiece) +
                   100 * Corners(myBoard, myTypePiece) +
                    10 * StablePiecesFromCorners(myBoard, myTypePiece) +
                    20 * Predictmyfuture(myBoard, myTypePiece) +
                     3 * PotentialMobility(myBoard, myTypePiece) +
                     5 * EdgeStability(myBoard, myTypePiece) +
                         NotMove(myBoard, myTypePiece);
        }
        else
        {
            //final game
            return 10 * CountPieces(myBoard, myTypePiece) +
                   50 * Mobility(myBoard, myTypePiece) +
                    5 * DirectionToVictory(myBoard, myTypePiece);
        }
    }
    int EvaluateEasy(TypePiece[,] myBoard, TypePiece myTypePiece)
    {
        int[,] weights = new int[8, 8]
        {
            //0     1    2      3   4   5      6     7  
            { 200, -500, 100,  50,  50, 100, -500,  200     }, //0
            { -500, -500, -50, -50, -50, -50, -500, -500    }, //1
            { 100,  -50, 100,   0,   0, 100,  -50,  100     }, //2
            { 50,  -50,   0,   0,   0,   0,  -50,   50      }, //3
            { 50,  -50,   0,   0,   0,   0,  -50,   50      }, //4
            { 100,  -50, 100,   0,   0, 100,  -50,  100     }, //5
            { -500, -500, -50, -50, -50, -50, -500, -500    }, //6
            { 200, -500, 100,  50,  50, 100, -500,  200     }, //7
        };
        //corner 0,0
        if (myBoard[0, 0] == myTypePiece)
        {
            weights[1, 0] = 100;
            weights[0, 1] = 100;
            weights[1, 1] = 100;
            weights[2, 1] = 100;
            weights[3, 1] = 100;
            weights[1, 2] = 100;
            weights[1, 3] = 100;
        }
        //corner 0,7
        if (myBoard[0, 7] == myTypePiece)
        {
            weights[6, 0] = 100;
            weights[7, 1] = 100;
            weights[6, 1] = 100;
            weights[5, 1] = 100;
            weights[4, 1] = 100;
            weights[6, 2] = 100;
            weights[6, 3] = 100;
        }
        //corner 7,0
        if (myBoard[7, 0] == myTypePiece)
        {
            weights[1, 7] = 100;
            weights[0, 7] = 100;
            weights[0, 6] = 100;
            weights[1, 6] = 100;
            weights[2, 6] = 100;
            weights[3, 6] = 100;
            weights[1, 4] = 100;
            weights[1, 5] = 100;
        }
        //corner 7,7
        if (myBoard[7, 7] == myTypePiece)
        {
            weights[6, 7] = 100;
            weights[7, 6] = 100;
            weights[6, 6] = 100;
            weights[5, 6] = 100;
            weights[4, 6] = 100;
            weights[6, 4] = 100;
            weights[6, 5] = 100;
        }

        int value = 0;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (myBoard[i, j] == TypePiece.NULL)
                    continue;
                if (myBoard[i, j] == myTypePiece)
                {
                    value += weights[i, j];
                }

            }
        }
        return value;

    }
    int EvalueteSoEasy(TypePiece[,] myBoard, TypePiece myTypePiece)
    {
        return 10*CountPieces(myBoard, myTypePiece) + 50*Corners(myBoard,myTypePiece);
    }
    int NotMove(TypePiece[,] myboard, TypePiece myTypePiece)
    {
        int value = 0;
        int[] weights = new int[4]
        {
            -100,-100,-100,-100
        };
        if(myboard[0,0] == myTypePiece)
           weights[0] = 50;
        if(myboard[0,7] == myTypePiece)
           weights[1] = 50;
        if(myboard[7,7] == myTypePiece)
           weights[2] = 50;
        if (myboard[7, 0] == myTypePiece)
           weights[3] = 50;

        //01,11,10
        if (myboard[0,1] == myTypePiece)
            value += weights[0];
        if (myboard[1, 1] == myTypePiece)
            value += weights[0];
        if (myboard[0, 1] == myTypePiece)
            value += weights[0];
        //06,16,17
        if (myboard[0, 6] == myTypePiece)
            value += weights[1];
        if (myboard[1, 6] == myTypePiece)
            value += weights[1];
        if (myboard[1, 7] == myTypePiece)
            value += weights[1];
        //67,66,76
        if (myboard[6, 7] == myTypePiece)
            value += weights[2];
        if (myboard[6, 6] == myTypePiece)
            value += weights[2];
        if (myboard[7, 6] == myTypePiece)
            value += weights[2];
        //60,61,71
        if (myboard[6, 0] == myTypePiece)
            value += weights[3];
        if (myboard[6, 1] == myTypePiece)
            value += weights[3];
        if (myboard[7, 1] == myTypePiece)
            value += weights[3];

        return value;
    }
    int PiecesOnBoard(TypePiece[,] myboard)
    {
        int count = 0;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (myboard[i, j] != TypePiece.NULL)
                    count++;
            }
        }
        return count;
    }
    int Mobility(TypePiece[,] myBoard, TypePiece myTypePiece)
    {
        TypePiece typeOpponent;
        if(myTypePiece == TypePiece.Black)
        {
            typeOpponent = TypePiece.White;
        }
        else
        {
            typeOpponent = TypePiece.Black;
        }
        
        List<Vector2Int> listMyCanMove = GetPosCanMoves(myBoard, myTypePiece);
        int myCanMove = listMyCanMove.Count;
        listMyCanMove.Clear();
        List<Vector2Int> listOpponentCanMove = GetPosCanMoves(myBoard,typeOpponent);
        int opponentCanMove = listOpponentCanMove.Count;
        listOpponentCanMove.Clear();

        if(opponentCanMove == 0)
        {
            return int.MaxValue;
        }
        else 
        {
            return  -1 * opponentCanMove;
        }
       
    }
    int CountPieces(TypePiece[,] myBoard, TypePiece myTypePiece)
    {
        int countBlack = 0;
        int countWhite = 0;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (myBoard[i, j] == TypePiece.NULL)
                    continue;
                if (myBoard[i, j] == TypePiece.Black)
                    countBlack++;
                else
                    countWhite++;
            }
        }

        if (myTypePiece == TypePiece.Black)
        {
            return (countBlack - countWhite);
        }
        else
        {
            return (countWhite - countBlack);
        }
    }
    int Corners(TypePiece[,] myBoard, TypePiece myTypePiece)
    {
        int value = 0;
        List<Vector2Int> corners = new List<Vector2Int>();
        corners.Add(new Vector2Int(0, 0));
        corners.Add(new Vector2Int(0, 7));
        corners.Add(new Vector2Int(7, 0));
        corners.Add(new Vector2Int(7, 7));

        foreach (var item in corners)
        {
            if (myBoard[item.x, item.y] == TypePiece.NULL)
                continue;
            if (myBoard[item.x, item.y] == myTypePiece)
            {
                value+=100;
            }
            
        }
        return value;
    }
    int DirectionToVictory(TypePiece[,] myBoard, TypePiece myTypePiece)
    {
        int count = 0;
        foreach (var item in myBoard)
        {
            if (item == myTypePiece)
                count++;
        }
        if (PiecesOnBoard(myBoard) == 64)
        {
            if (count >= 32)
                return 1000;
            else
                return -1000;
        }
        else
        {
            return count++;
        }
    }
    int StablePiecesFromCorners(TypePiece [,] myBoard, TypePiece myTypePiece)
    {
        int count = 0;
        if(myBoard[0,0] == myTypePiece)
        {
            for (int i = 1; i < 4; i++)
            {
                if (myBoard[0, i] == myTypePiece)
                    count++;
                if (myBoard[i, 0] == myTypePiece)
                    count++;
            }
        }
        if(myBoard[7,0] == myTypePiece)
        {
            for (int i = 1; i < 4; i++)
            {
                if (myBoard[7, i] == myTypePiece)
                    count++;
            }
            for (int i = 6; i > 3 ; i--)
            {
                if (myBoard[i, 0] == myTypePiece)
                    count++;
            }
        }
        if(myBoard[0,7] == myTypePiece)
        {
            for (int i = 1; i < 4; i++)
            {
                if (myBoard[i, 7] == myTypePiece)
                    count++;
            }
            for (int i = 6; i > 3; i--)
            {
                if (myBoard[0, i] == myTypePiece)
                    count++;
            }
        }
        if(myBoard[7,7] == myTypePiece)
        {
            for (int i = 6; i > 3; i--) 
            {
                if (myBoard[7, i] == myTypePiece)
                    count++;
                if (myBoard[i, 7] == myTypePiece)
                    count++;
            }
        }
        return count * 5;
    }
    int Predictmyfuture(TypePiece [,] myBoard, TypePiece myTypePiece)
    {
        List<Vector2Int> listOpponentCanMove;
        TypePiece TypeOpponent;
        //calc Position Opponent Can Moves
        if (myTypePiece == TypePiece.Black)
        {
            TypeOpponent = TypePiece.White;
            listOpponentCanMove = GetPosCanMoves(myBoard, TypeOpponent);
        }
        else
        {
            TypeOpponent = TypePiece.Black;
            listOpponentCanMove = GetPosCanMoves(myBoard, TypeOpponent);
        }
        foreach (var item in listOpponentCanMove)
        {
            TypePiece[,] _cloneBoard = cloneBoard(myBoard);
            MakeAMove(_cloneBoard, item, TypeOpponent);
            List<Vector2Int> ListMyCanMove = GetPosCanMoves(_cloneBoard, myTypePiece);
            if(ListMyCanMove.Count == 0)
                return -1000;
        }
        return 1;
                   
    }
    int PotentialMobility(TypePiece [,] myBoard, TypePiece myTypePiece)
    {
        int count = 0;
        TypePiece opponentTypePiece;
        if (myTypePiece == TypePiece.Black)
            opponentTypePiece = TypePiece.White;
        else
            opponentTypePiece = TypePiece.Black;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if(myBoard[i,j] == opponentTypePiece)
                {
                    if(i - 1 > 0)
                    {
                        if (myBoard[i - 1, j] == TypePiece.NULL)
                            count++;
                    }
                    if(i + 1 < 8)
                    {
                        if (myBoard[i + 1, j] == TypePiece.NULL)
                            count++;
                    }
                    if(j - 1 > 0)
                    {
                        if (myBoard[i, j - 1] == TypePiece.NULL)
                            count++;
                    }
                    if(j + 1 < 8)
                    {
                        if (myBoard[i, j + 1] == TypePiece.NULL)
                            count++;
                    }
                }
            }
        }
        return count * 3;
    }
    //Edge
    bool Isfilled(TypePiece[,] myBoard, int edge)
    {
        switch (edge)
        {
            case 1:
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (myBoard[i, 0] == TypePiece.NULL)
                            return false;
                    }
                    return true;
                }
            case 2:
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (myBoard[0, i] == TypePiece.NULL)
                            return false;
                    }
                    return true;
                }
            case 3:
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (myBoard[i, 7] == TypePiece.NULL)
                            return false;
                    }
                    return true;
                }
            default:
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (myBoard[7, i] == TypePiece.NULL)
                            return false;
                    }
                    return true;
                }
        }
    }
    bool ProbabilityFlip(TypePiece[,] myBoard, Vector2Int pos, int edge)
    {
        TypePiece myPiece = myBoard[pos.x, pos.y];
        switch (edge)
        {
            case 1:
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (myBoard[i, 0] != TypePiece.NULL && myBoard[i, 0] != myPiece)
                            return true;
                    }
                    return false;
                }
            case 2:
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (myBoard[0, i] != TypePiece.NULL && myBoard[0, i] != myPiece)
                            return true;
                    }
                    return false;
                }
            case 3:
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (myBoard[i, 7] != TypePiece.NULL && myBoard[i, 7] != myPiece)
                            return true;
                    }
                    return false;
                }
            default:
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (myBoard[7, i] != TypePiece.NULL && myBoard[7, i] != myPiece)
                            return true;
                    }
                    return false;
                }
        }
    }
    int EdgeStability(TypePiece [,] myBoard, TypePiece myTypePiece)
    {
        int value = 0;
        for (int i = 1; i <= 4; i++)
        {
            if (Isfilled(myBoard, i))
                continue;

            switch (i)
            {
                case 1:
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            if(myBoard[j,0] == myTypePiece)
                            {
                                if (ProbabilityFlip(myBoard, new Vector2Int(j, 0), 1))
                                {
                                    value++;
                                    break;
                                }
                            }      
                        }
                        break;
                    }
                case 2:
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            if (myBoard[0, j] == myTypePiece)
                            {
                                if (ProbabilityFlip(myBoard, new Vector2Int(0, j), 2))
                                {
                                    value++;
                                    break;
                                }
                            }
                        }
                        break;
                    }
                case 3:
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            if (myBoard[j, 7] == myTypePiece)
                            {
                                if (ProbabilityFlip(myBoard, new Vector2Int(j, 7), 3))
                                {
                                    value++;
                                    break;
                                }
                            }
                        }
                        break;
                    }
                default:
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            if (myBoard[7, j] == myTypePiece)
                            {
                                if (ProbabilityFlip(myBoard, new Vector2Int(7, j), 4))
                                {
                                    value++;
                                    break;
                                }
                            }
                        }
                        break;
                    }
            }
        }
        return value * 10;
    }
}