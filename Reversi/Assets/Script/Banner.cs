﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Banner : MonoBehaviour {
    public static Banner Instance { get; private set; }

    public CanvasGroup bannerCanvasGroup;
    private float bannerCountDown;
    private float bannerTime;

    // Use this for initialization
    void Awake () {
        Instance = this;

        //in second
        bannerTime = 2.5f;
    }
	
	// Update is called once per frame
	void Update () {
        UpdateBanner();
    }

    public void ShowBanner(string message)
    {
        bannerCanvasGroup.GetComponentInChildren<Text>().text = message;

        bannerCountDown = bannerTime;
    }

    private void UpdateBanner()
    {
        if (bannerCountDown > 0.0f)
        {
            bannerCanvasGroup.alpha = bannerCountDown / bannerTime;
            bannerCountDown -= Time.deltaTime;
        }
        else
        {
            bannerCanvasGroup.alpha = 0f;
        }
    }
}
