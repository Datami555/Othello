﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class About : MonoBehaviour {

    public static About Instance { get; private set; }

    public Mover camMover;

    public GameObject UITLogo;
    public TMPro.TMP_Text GVHD;

    public ParticleSystem plasmaExplosion;

    public bool isOpeningAbout { get; private set; }

    private bool hasDisplayedGVHD;

    private float radius = 10f;
    private float explosionForce = 500f;

    // Use this for initialization
    void Start () {
        Instance = this;
        isOpeningAbout = false;
        hasDisplayedGVHD = false;
        GVHD.alpha = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        if (isOpeningAbout)
        {
            if (!hasDisplayedGVHD)
            {
                StartCoroutine(ShowInfo());
            }
            else
            {
                StartCoroutine(BlendGVHD());
            }
        }
        if (GVHD.alpha == 1f && Input.GetMouseButtonDown(2))
        {
            PlayExplosion();
        }
    }

    public void OpenAbout()
    {
        if (!isOpeningAbout)
        {
            camMover.OpenAbout();
            isOpeningAbout = true;
        }
        else
        {
            camMover.LeaveAbout();
            isOpeningAbout = false;
        }

    }

    private IEnumerator ShowInfo()
    {
        for (float i = 0; i < 1f; i += Time.deltaTime)
        {
            yield return null;
        }

        if (!hasDisplayedGVHD)
        {
            PlayExplosion();
            hasDisplayedGVHD = true;
        }
    }

    private void PlayExplosion()
    {
        plasmaExplosion.Play();
        camMover.Shake(3f, 5f, 1f, 1f);

        Collider[] colliders = Physics.OverlapSphere(UITLogo.transform.position, radius);

        foreach (Collider nearbyObj in colliders)
        {
            Rigidbody rigidbody = nearbyObj.GetComponent<Rigidbody>();

            rigidbody.AddExplosionForce(explosionForce, UITLogo.transform.position, radius);
        }
    }

    private IEnumerator BlendGVHD()
    {
        if (GVHD.alpha == 1.0f)
        {
            yield break;
        }

        for (float i = 0; i <= 1f; i += (Time.deltaTime * 0.5f)) 
        {
            GVHD.alpha += i;
            yield return null;
        }
        GVHD.alpha = 1f;
    }
}
