﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public partial class ChessBoard : MonoBehaviour
{
    private string conn = @"F:\Code\Unity\Othello\Reversi\Assets\Script\Recource\Book.txt";

    public string ScriptAgree(string _script)
    {
        foreach (var item in ReadBook())
        {

            string[] result = item.Split(' ');
            string script = result[0];
            string goAway = result[1];

            if (script.CompareTo(_script) == 0)
            {
                return goAway;
            }
        }
        return null;
    }
    string[] ReadBook()
    {
       string[] Lines = File.ReadAllLines(conn);

       return Lines;
    }
}
