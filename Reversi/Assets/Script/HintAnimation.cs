﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintAnimation : MonoBehaviour {
    private bool isEnableAnimation;
    private Renderer rend;
    private float speed;

    public bool IsEnableAnimation
    {
        get
        {
            return isEnableAnimation;
        }

        set
        {
            isEnableAnimation = value;
        }
    }

    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();

        speed = 0.3f;
    }
	
	// Update is called once per frame
	void Update () {
        if (IsEnableAnimation)
        {
            float metallic = 0.2f + Mathf.PingPong(Time.time * speed, 0.4f);
            rend.material.SetFloat("_Metallic", metallic);
        }
        
    }
}
