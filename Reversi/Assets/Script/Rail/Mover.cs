﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

    public Rail rail;
    public PlayMode mode;

    public float speed = 100.0f;
    public bool isReversed;
    public bool isLooping;
    public bool Pingpong;

    public GameObject aboutViewObj;
    private Vector3 prePos;
    private Quaternion preRotation;
    private bool isOpeningAbout = false;
    private bool isMovingToAbout;

    private int currentSeg;
    private float transition;
    private bool isCompleted;

    private void Update()
    {
        if (!rail)
        {
            return;
        }

        if (!isCompleted)
        {
            Play(!isReversed);
        }
        else
        {
            if (isOpeningAbout)
            {
                StartCoroutine(MoveToAboutView(isMovingToAbout));
            }
        }

        
    }

    private void Play(bool forward = true)
    {
        transition += Time.deltaTime * rail.Magnitude(currentSeg) * speed * (forward ? 1 : -1);

        if (transition > 1)
        {
            transition = 0;
            currentSeg++;

            if (currentSeg == rail.nodes.Count)
            {
                if (isLooping)
                {
                    if (Pingpong)
                    {
                        transition = 1;
                        currentSeg = rail.nodes.Count - 1;
                        isReversed = !isReversed;
                    }
                    else
                    {
                        currentSeg = 0;
                    }
                }
                else
                {
                    isCompleted = true;
                    return;
                }
            }
        }
        else if (transition < 0)
        {
            transition = 1;
            currentSeg--;

            if (currentSeg == -1)
            {
                if (isLooping)
                {
                    if (Pingpong)
                    {
                        transition = 0;
                        currentSeg = 0;
                        isReversed = !isReversed;
                    }
                    else
                    {
                        currentSeg = rail.nodes.Count - 1;
                    }
                }
                else
                {
                    isCompleted = true;
                    return;
                }
            }
        }

        transform.position = rail.PositionOnRail(currentSeg, transition, mode);
        transform.rotation = rail.Orientation(currentSeg, transition);
    }

    public void OpenAbout()
    {
        isCompleted = true;
        prePos = transform.position;
        preRotation = transform.rotation;

        isMovingToAbout = true;
        isOpeningAbout = true;
    }

    public void LeaveAbout()
    {
        isOpeningAbout = true;
        isMovingToAbout = false;
    }

    private IEnumerator MoveToAboutView(bool isMovingToAbout)
    {
        if (isMovingToAbout)
        {
            if (transform.position == aboutViewObj.transform.position)
            {
                yield break;
            }

            for (float t = 0; t <= 1; t += Time.deltaTime * 3f)
            {
                transform.position = Vector3.Lerp(prePos, aboutViewObj.transform.position, t);
                transform.rotation = Quaternion.Lerp(preRotation, aboutViewObj.transform.rotation, t);

                yield return null;
            }

            transform.position = aboutViewObj.transform.position;
            transform.rotation = aboutViewObj.transform.rotation;

            isOpeningAbout = false;
        }
        else
        {
            for (float t = 0; t < 1; t += Time.deltaTime * 3f)
            {
                transform.position = Vector3.Lerp(aboutViewObj.transform.position, prePos, t);
                transform.rotation = Quaternion.Lerp(aboutViewObj.transform.rotation, preRotation, t);

                yield return null;
            }
            isCompleted = false;
            isOpeningAbout = false;

            transform.position = prePos;
            transform.rotation = preRotation;

            isOpeningAbout = false;
        }

    }

    public void Shake(float magnutite, float roughness, float fadein, float fadeout)
    {
        EZCameraShake.CameraShaker.Instance.ShakeOnce(magnutite, roughness, fadein, fadeout);
    }
}
