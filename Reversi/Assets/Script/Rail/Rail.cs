﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public enum PlayMode
{
    Linear,
    Catmull
}

[ExecuteInEditMode]
public class Rail : MonoBehaviour {
    public List<Transform> nodes;

    private void Start()
    {
        nodes = new List<Transform>();

        foreach (Transform child in transform)
        {
            nodes.Add(child);
        }

    }

    public Quaternion Orientation(int seg, float t)
    {
        Quaternion p1 = nodes[seg].rotation;
        Quaternion p2 = nodes[(seg + 1) % nodes.Count].rotation;

        return Quaternion.Lerp(p1, p2, t);
    }

    public Vector3 LinearPosition(int seg, float t)
    {
        Vector3 p1 = nodes[seg].position;
        Vector3 p2 = nodes[(seg + 1) % nodes.Count].position;

        return Vector3.Lerp(p1, p2, t);
    }

    public Vector3 CatmullPosition(int seg, float t)
    {
        Vector3 p1 = nodes[(seg + nodes.Count - 1) % nodes.Count].position;
        Vector3 p2 = nodes[seg % nodes.Count].position;
        Vector3 p3 = nodes[(seg + 1) % nodes.Count].position;
        Vector3 p4 = nodes[(seg + 2) % nodes.Count].position;

        float t2 = t * t;
        float t3 = t2 * t;

        float x = 0.5f * ((2.0f * p2.x)
            + (-p1.x + p3.x) * t
            + (2.0f * p1.x - 5.0f * p2.x + 4 * p3.x - p4.x) * t2
            + (-p1.x + 3.0f * p2.x - 3.0f * p3.x + p4.x) * t3);

        float y = 0.5f * ((2.0f * p2.y)
            + (-p1.y + p3.y) * t
            + (2.0f * p1.y - 5.0f * p2.y + 4 * p3.y - p4.y) * t2
            + (-p1.y + 3.0f * p2.y - 3.0f * p3.y + p4.y) * t3);

        float z = 0.5f * ((2.0f * p2.z)
            + (-p1.z + p3.z) * t
            + (2.0f * p1.z - 5.0f * p2.z + 4 * p3.z - p4.z) * t2
            + (-p1.z + 3.0f * p2.z - 3.0f * p3.z + p4.z) * t3);

        return new Vector3(x, y, z);
    }

    public Vector3 PositionOnRail(int seg, float t, PlayMode playMode)
    {
        switch (playMode)
        {
            default:
            case PlayMode.Linear:
                return LinearPosition(seg, t);
            case PlayMode.Catmull:
                return CatmullPosition(seg, t);
        }
    }

    public float Magnitude(int seg)
    {
        float m = (nodes[(seg + 1) % nodes.Count].position - nodes[seg].position).magnitude;
        return 1 / m;
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < nodes.Count; i++)
        {
            Handles.DrawDottedLine(nodes[i].position, nodes[(i + 1) % nodes.Count].position, 3.0f);
        }
    }
}
