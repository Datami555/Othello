﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardHint : MonoBehaviour {
    public static BoardHint Instance { get; set; }

    private Vector3 hintOffset;

    public GameObject hintPrefab;
    List<GameObject> hints;

    private void Start()
    {
        Instance = this;
        hints = new List<GameObject>();

        hintOffset = new Vector3(0.5f, 0.0f, 0.5f);
    }

    private GameObject GetHintObject()
    {
        GameObject g = hints.Find(h => !h.activeSelf);

        if (g == null)
        {
            g = Instantiate(hintPrefab);
            hints.Add(g);
        }

        return g;
    }

    public void HintValidMoves(List<Vector2Int> validMoves)
    {
        validMoves.ForEach(v =>
        {
            GameObject g = GetHintObject();
            g.SetActive(true);
            g.GetComponent<HintAnimation>().IsEnableAnimation = true;
            g.transform.position = new Vector3(v.x, 0.0f, v.y) + hintOffset - ChessBoard.Instance.BoardOffset;
        });
    }

    public void HideHint()
    {
        hints.ForEach(h =>
        {
            h.SetActive(false);
            h.GetComponent<HintAnimation>().IsEnableAnimation = false;
        });
    }
}
